# Mel spectrogram parameters:
N_MELS = 64
N_FFT = 1024
HOP_LENGTH = 512

# Dataset csv parameters:
CSV_COLUMNS = {
        "folder" : 0,
        "filename" : 1,
        "class_id" : 2,
        }

# General 
TEST_AUDIO_DIR = "/home/maxha/code/python/matchbox-ai/matchbox_testing_data"
TEST_ANNOTATIONS_FILE="/home/maxha/code/python/matchbox-ai/matchbox_testing_data/matchbox_testing_data.csv"
ANNOTATIONS_FILE="/home/maxha/code/python/matchbox-ai/matchbox_training_data/matchbox_training_data.csv"
AUDIO_DIR = "/home/maxha/code/python/matchbox-ai/matchbox_training_data/"
SAMPLE_RATE = 44100
NUM_SAMPLES = (SAMPLE_RATE * 1.5)

# Training
BATCH_SIZE = 128        
EPOCHS = 1000
LEARNING_RATE = 1e-3 
VALIDATION_INTERVAL = 20 
STATE_DICT_FILE = "resnet18_md.pth"

# Class mappings
CLASS_MAPPING = [
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten",
]
"""
CLASS_MAPPING = [
        "zero",
        "five",
        "ten",
        "fifteen",
        "twenty",
        "twenty-five",
        "thirty",
        "thirty-five",
        "forty",
        "forty-five",
        "fifty"
]
"""
