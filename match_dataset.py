from torch.utils.data import Dataset
import pandas as pd
import torch
import torchaudio
import os
import matplotlib.pyplot as plt
from config import *


class MatchDataset(Dataset):

    def __init__(self, annotations_file, audio_dir, target_sample_rate, num_samples, device):
        self.annotations = pd.read_csv(annotations_file)
        self.audio_dir = audio_dir
        self.device = device
        self.target_sample_rate = target_sample_rate
        self.num_samples = num_samples
        self.transformation = torchaudio.transforms.MelSpectrogram(sample_rate=SAMPLE_RATE,
                                                                  n_fft=N_FFT,
                                                                  hop_length=HOP_LENGTH,
                                                                  n_mels=N_MELS)
        self.transformation = self.transformation.to(self.device)

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self,index):
        audio_sample_path = self._get_audio_sample_path(index)
        label = self._get_audio_sample_label(index)
        signal, sample_rate = torchaudio.load(audio_sample_path) 
        signal = signal.to(self.device) 
        signal = self._resample_if_necessary(signal, sample_rate)
        signal = self._mix_down_if_necessary(signal)
        signal = self._cut_if_necessary(signal)
        signal = self._right_pad_if_necessary(signal)
        signal = self.transformation(signal)
        signal = self._log_transform(signal)
        return signal, label

    def _log_transform(self, signal):
        log_transform = torchaudio.transforms.AmplitudeToDB()

        return log_transform(signal)


    def _get_audio_sample_path(self, index):
        folder = f"{self.annotations.iloc[index, CSV_COLUMNS['folder']]}" 
        file_name = f"{self.annotations.iloc[index, CSV_COLUMNS['filename']]}" 
        path = os.path.join(self.audio_dir, folder, file_name)
        return path

    def _get_audio_sample_label(self, index):
        return self.annotations.iloc[index, CSV_COLUMNS["class_id"]]

    def _resample_if_necessary(self, signal, sample_rate):
        if sample_rate != self.target_sample_rate: 
            resampler = torchaudio.transforms.Resample(sample_rate, self.target_sample_rate).to(self.device)
            signal = resampler(signal)
        return signal

    def _mix_down_if_necessary(self, signal):
        if signal.shape[0] > 1: 
            signal = torch.mean(signal, dim=0, keepdim=True)
        return signal

    def _cut_if_necessary(self, signal):
        if signal.shape[1] > self.num_samples:
            signal = signal[:, :self.num_samples] 

        return signal
    def _right_pad_if_necessary(self, signal):
        length_signal = signal.shape[1]
        if length_signal < self.num_samples:
            num_missing_samples = self.num_samples - length_signal
            last_dim_padding = (0, int(num_missing_samples)) 
            signal = torch.nn.functional.pad(signal, last_dim_padding)
        return signal


if __name__ == "__main__":
    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"

    print(f"using {device}")


    tmd = MatchDataset(ANNOTATIONS_FILE, AUDIO_DIR, SAMPLE_RATE, NUM_SAMPLES, device)

    signal, label = tmd[10]

    plt.figure()
    plt.imshow(signal.cpu().squeeze().numpy(), cmap='viridis')
    plt.show()
