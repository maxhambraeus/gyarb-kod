from pydub import AudioSegment
import csv
import os
from config import *


TEST_IN_FOLDER = "raw_test_data"
TEST_OUT_FOLDER = "matchbox_testing_data"
IN_FOLDER = "raw_sound_data"
OUT_FOLDER = "matchbox_training_data"
SLICE_LENGTH = int((NUM_SAMPLES / SAMPLE_RATE )*1000) #milliseconds
TRIM_OFF_START = 500 
TRIM_OFF_END = 500 
PRE_MAX = 3
POST_MAX = 3
PRE_AVG = 3
POST_AVG = 3
DELTA = 3
PEAK_PICK_WAIT = 3

TEST_CSV_FILE_NAME = f"{TEST_OUT_FOLDER}.csv"
CSV_FILE_NAME = f"{OUT_FOLDER}.csv"

def add_to_dataset_time_based(file_path, folder, out_folder, test_data, csv_file_name):
    with open(os.path.join(out_folder, csv_file_name), 'a') as csv_file:
        filewriter = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        in_file = AudioSegment.from_wav(file_path)
        duration = len(in_file)
        for i in range(TRIM_OFF_START, (duration-TRIM_OFF_END), (SLICE_LENGTH if test_data else int(SLICE_LENGTH / 10))):
            slice_start = i
            slice_end = i+SLICE_LENGTH

            audio_slice = in_file[slice_start:slice_end]


            uid = 0
            file_name = f"{folder}_{slice_start}_{slice_end}.wav"
            while file_name in os.listdir(os.path.join(out_folder, folder)):
                uid += 1
                file_name = f"{folder}_{slice_start}_{slice_end}_{uid}.wav"


            audio_slice.export(os.path.join(out_folder, folder, file_name), format="wav")
            filewriter.writerow([folder, file_name, int(int(folder))]) 

def add_files(test_data):
    if test_data:
        in_folder = TEST_IN_FOLDER
        out_folder = TEST_OUT_FOLDER
        csv_file_name = TEST_CSV_FILE_NAME
    else:
        in_folder = IN_FOLDER
        out_folder = OUT_FOLDER
        csv_file_name = CSV_FILE_NAME
    
    for f in os.listdir(in_folder):# create all out folders
        os.makedirs(f"{out_folder}/{f}", exist_ok=True)

    with open(os.path.join(out_folder, csv_file_name), 'w') as csv_file:
            filewriter = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            filewriter.writerow(["folder", "file_name", "class_id"])


    for folder in os.listdir(in_folder):# loop through every file in every in folder, and add them
        for file in os.listdir(os.path.join(in_folder, folder)):
            add_to_dataset_time_based(os.path.join(in_folder, folder, file), folder, out_folder, test_data, csv_file_name)


    

if __name__ == "__main__":
    
    add_files(True)
    add_files(False)

    


