import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.utils import data
from torch.utils.tensorboard import SummaryWriter
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn



from match_dataset import MatchDataset
from torchvision.models import resnet18, ResNet18_Weights

from config import *


def predict(model, input, target, class_mapping):
    model.eval()
    with torch.no_grad():
        predictions = model(input)
        predicted_index = predictions[0].argmax(0)
        predicted = class_mapping[predicted_index]
        expected = class_mapping[target]
    return predicted, expected

def make_confusion_matrix(model, data):
    model.eval()
    y_pred = []
    y_true = []
    for i in data:
        input, target = i[0], i[1]
        input.unsqueeze_(0) #magic

        predicted, expected = predict(model, input, target, CLASS_MAPPING)

        y_pred.append(predicted)
           
        y_true.append(expected)

    model.train()


    cf_matrix = confusion_matrix(y_true, y_pred)

    df_cm = pd.DataFrame(cf_matrix / np.sum(cf_matrix, axis=1)[:,None], index = [i for i in CLASS_MAPPING], columns = [i for i in CLASS_MAPPING])

    plt.figure(figsize = (10,7))
    return sn.heatmap(df_cm, annot=True).get_figure()



def create_data_loader(train_data, batch_size):
    train_dataloader = DataLoader(train_data, batch_size=batch_size)
    return train_dataloader

def train_single_epoch(model, train_data_loader, loss_fn, optimiser, device, current_epoch, writer):
    for input, target in train_data_loader:
        input, target = input.to(device), target.to(device)

        #calculate loss
        prediction = model(input)
        loss = loss_fn(prediction, target)

        #backpropogate error and update weights
        optimiser.zero_grad()
        loss.backward()
        optimiser.step()

        writer.add_scalar("Loss/Training", loss, current_epoch)


def get_accuracy(model, data, precision=0, verbose=False):
    model.eval()
    correct_predicted = 0
    total_predicted = 0
    for i in data:
        input, target = i[0], i[1]
        input.unsqueeze_(0) #magic

        predicted, expected = predict(model, input, target, CLASS_MAPPING)

        if abs(CLASS_MAPPING.index(predicted) - CLASS_MAPPING.index(expected)) <= precision: 
            correct_predicted += 1
        total_predicted += 1

        if verbose:
            print(f"predicted: {predicted}, expected: {expected}")

    model.train()
    return correct_predicted / total_predicted

def train(model, train_data_loader, loss_fn, optimiser, device, epochs, writer, val_data, train_data, test_data):
    for i in range(epochs):
        print(f"Epoch {i}")
        train_single_epoch(model, train_data_loader, loss_fn, optimiser, device, i, writer)
        print("---------------------------")

        if i % VALIDATION_INTERVAL == 0 or i == epochs-1:# every n epochs and the last one
            train_accuracy = get_accuracy(model, train_data)
            writer.add_scalar("Accuracy/Training Data", train_accuracy, i)

            val_accuracy = get_accuracy(model, val_data)
            writer.add_scalar("Accuracy/Validation Data", val_accuracy, i)

            testing_accuracy = get_accuracy(model, test_data)
            writer.add_scalar("Accuracy/Testing Data", testing_accuracy, i)

            writer.add_figure("Confusion matrix", make_confusion_matrix(model, test_data), i)

    print("Finished training")


if __name__ == "__main__":
    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"
    print(f"Using {device}")



    md = MatchDataset(ANNOTATIONS_FILE,
                            AUDIO_DIR,
                            SAMPLE_RATE,
                            NUM_SAMPLES,
                            device)

    test_data = MatchDataset(TEST_ANNOTATIONS_FILE,
                             TEST_AUDIO_DIR,
                             SAMPLE_RATE,
                             NUM_SAMPLES,
                             device)

    train_data, val_data = data.random_split(md, [0.85, 0.15])

    print(f"training with {len(train_data)} training samples")
    train_dataloader = create_data_loader(train_data, BATCH_SIZE)

    model = resnet18(weights=ResNet18_Weights.DEFAULT)
    model.conv1=nn.Conv2d(1, model.conv1.out_channels,
                      kernel_size=model.conv1.kernel_size[0],
                      stride=model.conv1.stride[0],
                      padding=model.conv1.padding[0])

    num_ftrs = model.fc.in_features

    model.fc = nn.Linear(num_ftrs, len(CLASS_MAPPING)) 
    model.to(device)


    loss_fn = nn.CrossEntropyLoss()
    optimiser = torch.optim.Adam(model.parameters(),
                                 lr=LEARNING_RATE)

    with SummaryWriter() as writer:
        writer.add_scalar("Parameters/Learning Rate", (LEARNING_RATE))
        writer.add_scalar("Parameters/Batch size", (BATCH_SIZE))

        train(model, train_dataloader, loss_fn, optimiser, device, EPOCHS, writer, val_data, train_data, test_data)

    torch.save(model.state_dict(), STATE_DICT_FILE)
    print("done")
